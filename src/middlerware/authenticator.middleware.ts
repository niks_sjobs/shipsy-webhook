import { HttpException, Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request, Response } from "express";

@Injectable()
export class AuthenticatorMiddleware implements NestMiddleware{

    use(req: Request, res: Response, next: NextFunction) {
        let accessToken = req.headers['authorization']
        if(!accessToken){
            //throw new HttpException('Authorization failed', 200)
            res.status(401)
            res.send({
                'status': 401,
                'message': 'Authorization failed'
            })
            
            return 
        }

        // Else check with introspection service, if this is valid access token
        next()
    }

}