import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { WebhookController } from "./controller/webhook.controller";
import { WebhookService } from "./service/webhook.service";
import { WebhookMockService } from "./service/webhook.mock.service";
import { WebhookServiceInterface } from "./interfaces/webhook.service.interface";
import { AuthenticatorMiddleware } from "src/middlerware/authenticator.middleware";

const providers = [
    {
        provide: WebhookServiceInterface,
        useClass: WebhookService,
    }
]

@Module({
    controllers: [WebhookController], 
    providers: providers, 
    exports: providers // This must be subset of providers
})
export class WebhookModule implements NestModule {

    configure(consumer: MiddlewareConsumer) {
        //consumer.apply(AuthenticatorMiddleware).forRoutes('*')
    }

}