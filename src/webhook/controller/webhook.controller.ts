import { Body, Controller, Get, Param, ParseIntPipe, Post, Req } from "@nestjs/common";
import { Request } from "express";
import { BodyDtoValidator } from "src/validator/bodydto.validator";
import { WebhookDto } from "../dto/webhook.dto";
import { WebhookEventDto } from "../dto/webhookevent.dto";
import { WebhookServiceInterface } from "../interfaces/webhook.service.interface";

@Controller('webhook')
export class WebhookController{

    /* This service variable can be assigned by any Service which implements WebhookServiceInterface */
    private service: WebhookServiceInterface

    constructor(/*@Inject('WebhookServiceInterface')*/ webhookService: WebhookServiceInterface) {
        this.service = webhookService
    }

    @Get('/events/all')
    async fetchWebhookEvents(): Promise<WebhookEventDto[]>{
        return await this.service.fetchAllWebhookEvents()
    }

    @Get('all')
    async fetchAllWebhooks(@Req() request: Request): Promise<WebhookDto[]>{
        let userId = parseInt(request.headers['authorization'])
        return await this.service.fetchAllWebhooks(userId)
    }

    @Get(':id')
    async getWebhook(@Param('id', ParseIntPipe) webhookId: number): Promise<WebhookDto> {
        // Make sure that user which is logged in is owner of this webhook
        return await this.service.fetchWebhook(webhookId)
    }

    @Post('add')
    async addNewWebhook(@Body(new BodyDtoValidator()) webhook: WebhookDto): Promise<Object>{
        // Make sure that user which is logged in is owner of this webhook
        return await this.service.addWebhook(webhook)
    }

    @Post('edit/:id')
    async editWebhook(@Param('id', ParseIntPipe) webhookId: number, @Body(new BodyDtoValidator()) webhook: WebhookDto) : Promise<Object>{
        // Make sure that user which is logged in is owner of this webhook
        return await this.service.editWebhook(webhookId, webhook)
    }

    @Post('delete/:id')
    async deleteWebhook(@Param('id', ParseIntPipe) webhookId: number) : Promise<Object>{
        // Make sure that user which is logged in is owner of this webhook
        return await this.service.deleteWebhook(webhookId)
    }

}