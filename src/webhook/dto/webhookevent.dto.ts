import { WebhookEventInterface } from "../interfaces/webhookevent.interface"

export class WebhookEventDto implements WebhookEventInterface{

    public id: number
    public name: string
    public displayName: string

    constructor(id: number, name: string, displayName: string){
        this.id = id
        this.name = name
        this.displayName = displayName
    }
} 