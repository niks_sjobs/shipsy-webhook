import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator"
import { WebhookInterface } from "../interfaces/webhook.interface"

export class WebhookDto implements WebhookInterface{

    @IsOptional()
    @IsNumber()
    public id?: number

    @IsNotEmpty()
    @IsString()
    public title: string

    @IsNotEmpty()
    @IsString()
    public url: string

    @IsNotEmpty()
    @Type(() => Array)
    public events: Array<string>

    constructor(id: number = null, title: string, url: string, events: Array<string>){
        this.id = id
        this.title = title
        this.url = url
        this.events = events
    }
} 