export interface WebhookEventInterface{
    id: number
    name: string
    displayName: string
}