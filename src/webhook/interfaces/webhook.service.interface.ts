import { Injectable } from "@nestjs/common";
import { WebhookInterface } from "./webhook.interface";
import { WebhookEventInterface } from "./webhookevent.interface";

@Injectable()
export abstract class WebhookServiceInterface{
    abstract fetchAllWebhookEvents(): Promise<Array<WebhookEventInterface>>
    abstract fetchAllWebhooks(userId: number): Promise<Array<WebhookInterface>>
    abstract fetchWebhook(webhookId: number): Promise<WebhookInterface | null>
    abstract addWebhook(webhook: WebhookInterface): Promise<Object>
    abstract editWebhook(webhookId: number, webhook: WebhookInterface): Promise<Object>
    abstract deleteWebhook(webhookId: number): Promise<Object>
}