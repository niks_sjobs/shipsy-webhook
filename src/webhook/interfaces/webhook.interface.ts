export interface WebhookInterface{
    id?: number
    title: string
    url: string
    events: Array<string>
}