import { Injectable } from "@nestjs/common";
import { WebhookDto } from "../dto/webhook.dto";
import { WebhookEventDto } from "../dto/webhookevent.dto";
import { WebhookInterface } from "../interfaces/webhook.interface";
import { WebhookServiceInterface } from "../interfaces/webhook.service.interface";
import { WebhookEventInterface } from "../interfaces/webhookevent.interface";

@Injectable()
export class WebhookMockService extends WebhookServiceInterface{

    /**
     * Function which fetch all events from database and return
     */

    fetchAllWebhookEvents(): Promise<WebhookEventInterface[]> {

        let events = [new WebhookEventDto(1, 'SOME_MOCK_EVENT', 'Some mock Event')]
        return new Promise((resolve, _) => {
            setTimeout(() => { resolve(events); }, 2000)
        })

    }


    fetchAllWebhooks(userId: number): Promise<WebhookInterface[]> {
        let webhooks = [
            new WebhookDto(1, "My 1st Mock Webhook", "https://www.moc.google.com", ["SOME_MOCK_EVENT"]),
            new WebhookDto(1, "My 2nd Mock Webhook", "https://www.moc.facebook.com", ["SOME_MOCK_EVENT"]),
        ]
        return new Promise((resolve, _) => {
            setTimeout(() => { resolve(webhooks) }, 2000)
        })
    }


    fetchWebhook(webhookId: number): Promise<WebhookInterface | null> {

        if (webhookId !== 1){
            return new Promise((resolve, _) => {
                resolve(null)
            })
        }

        let webhook = new WebhookDto(1, "My 1st Mock Webhook", "https://www.moc.google.com", ["SOME_MOCK_EVENT"])
        return new Promise((resolve, _) => {
            resolve(webhook)
        })
    }

    addWebhook(webhook: WebhookDto): Promise<Object>{
        throw new Error('Method does not implemented')
    }

    editWebhook(webhookId: number, webhook: WebhookInterface): Promise<Object>{
        throw new Error('Method does not implemented')
    }

    deleteWebhook(webhookId: number): Promise<Object>{
        throw new Error('Method does not implemented')
    }

}