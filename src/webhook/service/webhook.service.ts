import { HttpException, Injectable } from "@nestjs/common";
import { WebhookDto } from "../dto/webhook.dto";
import { WebhookEventDto } from "../dto/webhookevent.dto";
import { WebhookInterface } from "../interfaces/webhook.interface";
import { WebhookServiceInterface } from "../interfaces/webhook.service.interface";
import { WebhookEventInterface } from "../interfaces/webhookevent.interface";

@Injectable()
export class WebhookService extends WebhookServiceInterface{

    private webhooks = [
        new WebhookDto(1, "Google Webhook", "https://www.google.com", ["GOOGLE_CALLED"]),
        new WebhookDto(2, "Facebook Webhook", "https://www.facebook.com", ["FACEBOOK_CALLED"]),
        new WebhookDto(3, "Twitter Webhook", "https://www.twitter.com", ["TWITTER_CALLED"]),
        new WebhookDto(4, "Yahoo Webhook", "https://www.yahoo.com", ["YAHOO_CALLED"]),
    ]

    private webhookEvents = [
        new WebhookEventDto(1, 'GOOGLE_CALLED', 'Google page is refreshed'),
        new WebhookEventDto(2, 'FACEBOOK_CALLED', 'Facebook page is refreshed'),
        new WebhookEventDto(3, 'TWITTER_CALLED', 'Twitter page is refreshed'),
        new WebhookEventDto(4, 'YAHOO_CALLED', 'Yahoo page is refreshed'),
        new WebhookEventDto(5, 'APPLE_CALLED', 'Apple page is refreshed'),
        new WebhookEventDto(6, 'MICROSOFT_CALLED', 'Microsoft page is refreshed')
    ]

    /**
     * Function which fetch all events from database and return
     */

    public fetchAllWebhookEvents(): Promise<Array<WebhookEventInterface>> {

        return new Promise((resolve, _) => {
            resolve(this.webhookEvents)
        })

    }

    public fetchAllWebhooks(userId: number): Promise<Array<WebhookInterface>>{

        return new Promise((resolve, _) => {
            resolve(this.webhooks)
        })

    }

    fetchWebhook(webhookId: number): Promise<WebhookInterface | null> {

        let webhook: WebhookInterface = this.webhooks.filter(wh => wh.id === webhookId)[0] || null
        if(!webhook){
            throw new HttpException('No webhook exists with given id', 200)
        }
        return new Promise((resolve, _) => {
            resolve(webhook)
        })
    }

    addWebhook(webhook: WebhookDto): Promise<Object>{

        // Check if event is valid or not.
        let allEvents = this.webhookEvents.map(event => event.name)
        webhook.events.forEach(event => {
            if(!allEvents.includes(event)){
                throw new HttpException(`Event ${event} Does not exists`, 200)
            }
        })

        // Save new webhook in database and save id
        webhook.id = this.webhooks.length + 1

        // Add webhook to database
        this.webhooks.push(webhook)

        // return new webhook in response
        return new Promise((resolve, _) => {
            resolve({
                'status': 200,
                'message': 'Webhook added successfully',
                'webhook': webhook
            })
        })
    }

    editWebhook(webhookId: number, webhook: WebhookInterface): Promise<Object>{

        // Check if event is valid or not.
        let allEvents = this.webhookEvents.map(event => event.name)
        webhook.events.forEach(event => {
            if(!allEvents.includes(event)){
                throw new HttpException(`Event ${event} Does not exists`, 200)
            }
        })

        // Fetch old webhook
        let oldWebhook: WebhookInterface = this.webhooks.filter(wh => wh.id === webhookId)[0] || null

        // Give error if not exists
        if(!oldWebhook){
            throw new HttpException('No webhook found with given id', 200)
        }

        // Update data
        oldWebhook.title = webhook.title
        oldWebhook.url = webhook.url
        oldWebhook.events = webhook.events

        // return new webhook in response
        return new Promise((resolve, _) => {
            resolve({
                'status': 200,
                'message': 'Webhook updated successfully',
                'webhook': oldWebhook
            })
        })
    }

    deleteWebhook(webhookId: number): Promise<Object>{
        let indexOfWebhook = this.webhooks.findIndex(webhook => webhook.id === webhookId)
        if(indexOfWebhook === -1){
            throw new HttpException('No webhook found with given id', 200)
        }

        this.webhooks.splice(indexOfWebhook, 1)

        // return new webhook in response
        return new Promise((resolve, _) => {
            resolve({
                'status': 200,
                'message': 'Webhook deleted successfully'
            })
        }) 
    }
    
}