import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { exec } from 'child_process';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  await app.listen(3000);
  console.log('All set now')
  exec(`open -a "Google Chrome" http://localhost:3000/webhook/all`)
}
bootstrap();
